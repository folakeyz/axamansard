import * as React from 'react';
import style from '../axa.module.scss';

const Links = () => {
    return(
    <div  className={style.links}>
        <h4>Quick Links</h4>
        <a>Process Maker</a>
        <a>Fintrak</a>
        <a>Unitrak</a>
        <a>HR Workplace</a>
        <a>AIMS Web</a>
        <a>AIMS Friendly</a>
        <a>Workbench</a>
        <a>Dynamics CRM</a>
        <a>Liquidation Workflow</a>
        <a>IT Service Desk</a>
        <a>Claims Workflow</a>
        <a>Calculator</a>
    </div>
    );
    }
    export default Links;